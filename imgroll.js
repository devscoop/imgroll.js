/*!
 * imgRoll.js
 *
 * Copyright 2012,
   This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * Date: Wed Feb 22 2012, 10:20AM 
 */
(function($){
    $.fn.extend({
        imgRoll: function(options) {
            var defaults = {
                transition: 5000,
		linger : 400,
		prefix : ""
            };
            var opts = $.extend(defaults, options);
            return this.each(function() {
                var a = new Array();  
		var t,i=0,l,h,w;		
		tr = opts.transition;
		lin = opts.linger;
		pre = opts.prefix;
		w = $(this).width();
		h = $(this).height();
		$(this).find('img').map(function() {
			$(this).css('display','none');
			$(this).attr('height',h);
			$(this).attr('width',w);
			a.push($(this).attr('src'));
		}).get();
		l = a.length;
		$("#"+pre+"1").css('display','block');
		if(l>1) {
			function roller() {	
				t = "#"+pre+(i+1);				
				$(t).fadeIn(200);
				$(t).attr('src',a[i]);
				$(t).delay(tr-(lin+300)).fadeOut(lin);
				i = (i==(l-1))?0:(i+1);
			}
			roller();	
			setInterval(roller,tr);
		}
            });
        }
    });
})(jQuery);
